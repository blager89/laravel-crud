@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        <div class="row">
            <div class="col-12">
            <div class="ticketsTitle text-center">
                <span >Title: </span><h3>{{$ticket->title}}</h3>
            </div>
            </div>

        </div>
            <hr>
        <div class="row">
            <div class="col-12">
            <div class="ticketsDescription text-center">
                <span >Title: </span><h3>{{$ticket->description}}</h3>
            </div>
            </div>
        </div>
    </div>
@endsection